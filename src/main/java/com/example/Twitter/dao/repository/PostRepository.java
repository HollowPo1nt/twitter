package com.example.Twitter.dao.repository;

import com.example.Twitter.dao.model.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PostRepository extends CrudRepository<Post, Long> {


}
