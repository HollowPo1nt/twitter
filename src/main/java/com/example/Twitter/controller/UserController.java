package com.example.Twitter.controller;

import com.example.Twitter.ResourceNotFoundException;
import com.example.Twitter.dao.model.User;
import com.example.Twitter.dao.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/users")
public class UserController {

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @GetMapping("")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Iterable<User> list() {
        return userRepository.findAll();
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)

    @ResponseBody
    public User create(@RequestBody User user) {
//        for (User u:userRepository.findAll()) {
//            if(u.getUsername().equals(user.getUsername())){
//                return user;
//            }
//            else return userRepository.save(user);
//        }
        //return null;
        return userRepository.save(user);
    }


    @GetMapping("/{Id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public User getUser(@PathVariable Long id) throws ResourceNotFoundException {
        return userRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @PutMapping("/{username}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public User updateUserByUsername(@PathVariable String username, @RequestBody User updatedUser) {    //
        for (User u : userRepository.findAll()) {
            if (u.getUsername().equals(username)) {
                u = updatedUser;
//                userRepository.delete(u);
//                userRepository.save(updatedUser);
                break;
            }
        }
        return updatedUser;
    }

    @PutMapping("/{Id}")
    @ResponseBody
    public User updateUserById(@PathVariable Long id, @RequestBody User updatedUser) throws ResourceNotFoundException {
        User user = userRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        user.setFirstName(updatedUser.getFirstName());
        user.setSecondName(updatedUser.getSecondName());
        user.setUsername(updatedUser.getUsername());

        user = userRepository.save(user);
        return user;
    }


    @DeleteMapping("/{username}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public User deleteUser(@PathVariable String username) {
        User user = null;
        for (User u : userRepository.findAll()
        ) {
            if (u.getUsername().equals(username))
                user = u;
        }
        userRepository.delete(user);
        return user;
    }


}
